var $root = $('html, body');
$('.scroll-down').click(function(e) {
    e.preventDefault();
    $root.animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    return false;
});