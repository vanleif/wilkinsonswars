<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/vendor/deployer/recipes/recipe/slack.php';
require __DIR__ . '/vendor/deployer/deployer/recipe/common.php';

// Set configurations
\Deployer\set('application', 'wilkinsonwars.pl');
\Deployer\set('repository', 'git@bitbucket.org:vanleif/wilkinsonswars.git');

\Deployer\set('slack_webhook', 'https://hooks.slack.com/services/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');

\Deployer\task('deploy', [
	'deploy:info',
	'deploy:prepare',
	'deploy:lock',
	'deploy:release',
	'deploy:update_code',
	'deploy:clear_paths',
	'deploy:shared',
	'deploy:writable',
	'deploy:symlink',
	'deploy:unlock',
	'cleanup',
])->desc('Deploy your project');

// Display success message on completion
\Deployer\after('deploy', 'success');
\Deployer\before('deploy', 'slack:notify');
\Deployer\after('deploy', 'slack:notify:success');

// Configure servers
\Deployer\host('wilkinsonstarwars.hades.vanleif.com')
	->user('wilkinson')
	->forwardAgent()
	->set('deploy_path', '/home/wilkinson/starwars')
	->set('bin/php', '/usr/bin/php7.0')
	->stage('wilkinsonstarwars.hades.vanleif.com');

